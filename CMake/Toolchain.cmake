message("Toolchain.cmake running.")

# Target operating system
message("Setting : CMAKE_SYSTEM_NAME/VERSION/PROCESSOR")

#
set(CMAKE_SYSTEM_NAME		Generic)
set(CMAKE_SYSTEM_VERSION	1)
set(CMAKE_SYSTEM_PROCESSOR	arm-eabi)

# Compilers to use for ASM, C and C++
message("Setting : Toolchain")
set(CMAKE_C_COMPILER		arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER		arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER		arm-none-eabi-g++)
set(CMAKE_OBJCOPY			arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP			arm-none-eabi-objdump)
set(CMAKE_SIZE			    arm-none-eabi-size)

# Initialse these options to blank (thats what happens if an unknown build type is selected)
set(MSG_LEN 0)
set(OPTIMISATION "-O0")
set(DEBUG "")
set(ADDITIONAL_FLAGS "")
set(IS_RELEASE TRUE)

# This selects the compiler flags, feel free to change these :)
message("Build mode : " ${CMAKE_BUILD_TYPE})
if (CMAKE_BUILD_TYPE STREQUAL "Release")
    message("Setting up Release flags.")
    set(OPTIMISATION	    "-O3")
    set(ADDITIONAL_FLAGS    "-flto -fomit-frame-pointer -Wl,--strip-all -fdevirtualize-speculatively")

elseif (CMAKE_BUILD_TYPE STREQUAL "MinSizeRel")
    message("Setting up MinSizeRel flags.")
    set(OPTIMISATION	    "-Os")
    set(ADDITIONAL_FLAGS    "-flto -fomit-frame-pointer -Wl,--strip-all -fdevirtualize-speculatively")

elseif (CMAKE_BUILD_TYPE STREQUAL "Debug")
    message("Setting up Debug flags.")
    set(OPTIMISATION	    "-Og")
    set(DEBUG               "-g3 -ggdb")

elseif (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
    message("Setting up RelWithDebInfo flags.")
    set(OPTIMISATION	    "-O3")
    set(DEBUG               "-g3 -ggdb")

elseif (CMAKE_BUILD_TYPE STREQUAL "MinSizeRelWithDebInfo")
    message("Setting up MinSizeRelWithDebInfo flags.")
    set(OPTIMISATION	    "-Os")
    set(DEBUG               "-g3 -ggdb")

else()
    message("WARNING - Unknown build configuration.")
endif()

#-march=${ARCH} 
#set(COMMON_FLAGS "-mfpu=${FPU} -mfloat-abi=${FLOAT-ABI} -mcpu=${CORE} -${ARM_ASM} ${OPTIMISATION} ${DEBUG} ${ADDITIONAL_FLAGS} -pedantic -Wall -Wextra -Wfloat-equal -Wshadow -Wall -Wl,--gc-sections -fmessage-length=${MSG_LEN} -ffunction-sections -fdata-sections -ffreestanding -fno-builtin")
set(COMMON_FLAGS "-mfpu=${FPU} -mfloat-abi=${FLOAT-ABI} -mcpu=${CORE} -${ARM_ASM} ${OPTIMISATION} ${DEBUG} ${ADDITIONAL_FLAGS} -pedantic -Wall -Wextra -Wfloat-equal -Wshadow -Wall -Wl,--gc-sections -fmessage-length=${MSG_LEN} -ffunction-sections -fdata-sections -ffreestanding -fno-builtin")
set(CPP_FLAGS "-fno-rtti -fno-exceptions -fno-use-cxa-atexit -fno-threadsafe-statics -ftemplate-backtrace-limit=0")

# 
set(CMAKE_ASM_FLAGS	        "${COMMON_FLAGS} -MP -MD -x assembler-with-cpp" CACHE INTERNAL "asm compiler flags")
set(CMAKE_ASM_FLAGS	        "${COMMON_FLAGS}" CACHE INTERNAL "asm compiler flags")
set(CMAKE_C_FLAGS           "${COMMON_FLAGS}" CACHE INTERNAL "c compiler flags")
set(CMAKE_CXX_FLAGS	        "${COMMON_FLAGS} ${CPP_FLAGS}" CACHE INTERNAL "cpp compiler flags")

# You may have a linker issue. If that occurs change nano.specs to nosys.specs
set(LN_FLAGS              "-lc -lm -lnosys ")
#   ,--cref -Wl ,--gc-sections
set(CMAKE_EXE_LINKER_FLAGS  "${COMMON_FLAGS} --specs=nosys.specs -lc -lm -lnosys -Wl,-Map=\"build.map\",--cref -Wl,--gc-sections" CACHE INTERNAL "exe link flags")

#
message(${CMAKE_C_FLAGS})
message(${CMAKE_CXX_FLAGS})
message(${CMAKE_EXE_LINKER_FLAGS})

# 
message("CMAKE_C_FLAGS = "          ${CMAKE_C_FLAGS})
message("CMAKE_CXX_FLAGS = "        ${CMAKE_CXX_FLAGS})
message("CMAKE_EXE_LINKER_FLAGS = " ${CMAKE_EXE_LINKER_FLAGS})
#
# 
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)